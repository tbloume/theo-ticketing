import "antd/dist/antd.css";
import "../src/stories/stories.scss";
export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  // .storybook/preview.js

  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};
