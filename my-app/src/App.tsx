import { QueryClient, QueryClientProvider } from "react-query";
import { ReactElement } from "react";
import { ReactQueryDevtools } from "react-query/devtools";
import { MainLayout } from "./components";
import "./App.scss";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

const App = (): ReactElement => {
  return (
    <div className="app">
      <QueryClientProvider client={queryClient}>
        <MainLayout />
        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </div>
  );
};

export default App;
