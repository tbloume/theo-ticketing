import ReactDOM from "react-dom";
import "antd/dist/antd.css";
import "./index.css";
import "@fontsource/mulish";
import Routing from "./Routing";

ReactDOM.render(<Routing />, document.getElementById("root"));
