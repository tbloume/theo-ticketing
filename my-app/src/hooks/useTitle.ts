import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";

import { getTitle, getTitleOfIndex } from "../routes/routes";
import useBasePath from "./useBasePath";

export const useTitle = () => {
  const { pathname } = useLocation();
  const basePath = useBasePath(pathname);
  const [title, setTitle] = useState("");
  const [hasParams, setHasParams] = useState(false);

  useEffect(() => {
    if (pathname === "/") {
      setTitle(getTitleOfIndex() as string);
      setHasParams(false);
      //  Route without params (Review condition)
    } else if (pathname === `/${basePath}`) {
      setTitle(getTitle(basePath) as string);
      setHasParams(false);
      //  Route with params
    } else {
      setTitle(getTitle(basePath.slice(0, -1)) as string);
      setHasParams(true);
    }
  }, [basePath, pathname]);

  return { title, hasParams };
};
export default useTitle;
