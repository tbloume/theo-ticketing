import { useState } from "react";

const usePagination = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [pageLimit, setPageLimit] = useState(10);
  const [totalRecords, setTotalRecords] = useState(10);
  const lastIndex = currentPage * pageLimit;
  const range =
    (currentPage <= 1 ? 0 : (currentPage - 1) * pageLimit + 1).toString() +
    -(lastIndex >= totalRecords ? totalRecords : lastIndex).toString();

  return {
    currentPage,
    setCurrentPage,
    pageLimit,
    setPageLimit,
    totalRecords,
    setTotalRecords,
    lastIndex,
    range,
  };
};
export default usePagination;
