import { useParams } from "react-router-dom";

const useBasePath = (pathname: string) => {
  const params = useParams<Record<string, string>>();

  return Object.values(params).reduce(
    (path: string, param) => path.replace(`/${param}`, ""),
    pathname.replace("/", "")
  );
};
export default useBasePath;
