import { ReactElement } from "react";
import "./styles.css";

type JobOutput = {
  status: string;
  _id: string;
};

interface ColumnType<T> {
  title: string;
  key: string;
  dataIndex?: string;
  render?: (value: any, record: T) => ReactElement;
}

interface TableProps<T = any> {
  dataSource: any[];
  columns: ColumnType<T>[];
}

const Table = ({ dataSource, columns }: TableProps) => {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          border: "1px solid black",
          justifyContent: "space-between",
        }}
      >
        {columns.map(({ title, key }) => (
          <div
            key={key}
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "center",
              flex: "1",
            }}
          >
            {title}
          </div>
        ))}
      </div>
      {dataSource.map((obj) => (
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          {columns.map(({ dataIndex, render }) => (
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
                flex: 1,
              }}
            >
              {render && dataIndex && render(obj[dataIndex], obj)}
              {render && !dataIndex && render(null, obj)}
              {!render && dataIndex && obj[dataIndex]}
            </div>
          ))}
        </div>
      ))}
    </div>
  );
};

const App = () => {
  const data: JobOutput[] = [
    {
      status: "Success",
      _id: "my-job-1",
    },
    {
      status: "Failure",
      _id: "my-job-2",
    },
    {
      status: "Waiting",
      _id: "my-job-3",
    },
  ];

  const columns: ColumnType<JobOutput>[] = [
    {
      title: "Job Id",
      key: "column-job",
      dataIndex: "_id",
      render: (value, record) => {
        return (
          <div>
            <span
              style={{ color: record.status === "Success" ? "green" : "red" }}
            >
              {value}
            </span>
          </div>
        );
      },
    },
  ];

  return (
    <div>
      <Table dataSource={data} columns={columns} />
    </div>
  );
};

export default App;
