/* eslint-disable import/no-extraneous-dependencies */
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { useState } from "react";
import { Pagination, PaginationProps } from "../components";

export default {
  title: "Pagination",
  component: Pagination,
  argTypes: {
    onPreviousPage: {
      action: "clicked",
    },
    onNextPage: {
      action: "clicked",
    },
    onSelectLimit: {
      description: "Triggered when totalRecords is reached",
    },
  },
  parameters: {
    docs: {
      source: {
        language: "html",
        type: "auto",
      },
    },
  },
} as ComponentMeta<typeof Pagination>;

const Template: ComponentStory<typeof Pagination> = (args) => (
  <Pagination {...args} />
);

export const Prototype: ComponentStory<typeof Pagination> = (args) => {
  const [currentPage, setCurrentPage] = useState(1);
  if (currentPage < 1) {
    setCurrentPage(1);
  }
  const [pageLimit, setPageLimit] = useState(10);
  return (
    <Pagination
      {...args}
      currentPage={currentPage}
      pageLimit={pageLimit}
      onPreviousPage={() => setCurrentPage(currentPage - 1)}
      onNextPage={() => setCurrentPage(currentPage + 1)}
      onSelectLimit={(value: string) => {
        setCurrentPage(1);
        setPageLimit(parseInt(value, 10));
      }}
    />
  );
};
Prototype.argTypes = {
  currentPage: {
    table: {
      disable: true,
    },
  },
  pageLimit: {
    table: {
      disable: true,
    },
  },
};
Prototype.parameters = {
  controls: {
    expanded: true,
  },
};
export const FirstPage = Template.bind({});

FirstPage.args = {
  currentPage: 1,
  pageLimit: 10,
  totalRecords: 58,
};

export const LastPage = Template.bind({});

LastPage.args = {
  currentPage: 6,
  pageLimit: 10,
  totalRecords: 58,
};

export const SmallCount = Template.bind({});

SmallCount.args = {
  currentPage: 1,
  pageLimit: 10,
  totalRecords: 6,
};

export const LargeCount = Template.bind({});

LargeCount.args = {
  currentPage: 1,
  pageLimit: 100,
  totalRecords: 1000,
};

// export const onlyRequiredArgs = Template.bind({});

// onlyRequiredArgs.args = {
//   name: "TAG",
// };
