// eslint-disable-next-line import/no-extraneous-dependencies
import { ComponentStory, ComponentMeta } from "@storybook/react";

import React from "react";
// eslint-disable-next-line import/no-extraneous-dependencies

import { Tag, TagProps } from "../components";

export default {
  title: "Tag",
  component: Tag,

  parameters: {
    docs: {
      source: {
        language: "yml", //  issue with html lang
        type: "auto",
      },
    },
  },
} as ComponentMeta<typeof Tag>;

const Template: ComponentStory<typeof Tag> = (args) => <Tag {...args} />;

export const Clear = Template.bind({});

Clear.args = {
  value: "TAG",
  color: "white",
};

export const Green = Template.bind({});

Green.args = {
  value: "TAG",
  color: "green",
};
export const Amber = Template.bind({});

Amber.args = {
  value: "TAG",
  color: "amber",
};
export const Red = Template.bind({});

Red.args = {
  value: "TAG",
  color: "red",
};
export const CustomStyle = Template.bind({});

CustomStyle.args = {
  value: "TAG",
  className: "custom-class",
};
CustomStyle.parameters = {
  backgrounds: { default: "dark" },
  docs: {
    description: {
      story: "**Customize** Tag component with your classes",
    },
  },
};
