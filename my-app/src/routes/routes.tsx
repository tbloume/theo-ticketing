import {
  BulbFilled,
  CodepenSquareFilled,
  FundFilled,
  PieChartFilled,
  SettingFilled,
} from "@ant-design/icons";
import { ReactElement } from "react";
import {
  AnalyzerDetails,
  AnalyzersList,
  JobDetails,
  JobsList,
  Overview,
  ResponderDetails,
  RespondersList,
  SettingsOverview,
} from "../components";

export type RouteObject = {
  title: string;
  path: string;
  element: ReactElement;
  isMenuItem?: boolean;
  isIndex?: boolean;
  icon?: ReactElement;
};

const routes: Record<string, RouteObject | null> = {
  overview: {
    title: "Overview",
    path: "/overview",
    element: <Overview />,
    isMenuItem: true,
    isIndex: true,
    icon: <PieChartFilled />,
  },

  jobs: {
    title: "Jobs",
    path: "/jobs",
    isMenuItem: true,
    element: <JobsList />,
    icon: <BulbFilled />,
  },
  job: {
    title: "Job",
    path: `/jobs/:id`,
    isMenuItem: false,
    element: <JobDetails />,
  },
  responders: {
    title: "Responders",
    path: "/responders",
    isMenuItem: true,
    element: <RespondersList />,
    icon: <CodepenSquareFilled />,
  },
  responder: {
    title: "Responder",
    path: `/responders/:id`,
    isMenuItem: false,
    element: <ResponderDetails />,
  },
  analyzers: {
    title: "Analyzers",
    path: "/analyzers",
    isMenuItem: true,
    element: <AnalyzersList />,
    icon: <FundFilled />,
  },
  analyzer: {
    title: "Analyzer",
    path: `/analyzers/:id`,
    isMenuItem: false,
    element: <AnalyzerDetails />,
  },
  dividerItem: null,
  settings: {
    title: "Settings",
    path: "/settings",
    isMenuItem: true,
    element: <SettingsOverview />,
    icon: <SettingFilled />,
  },
};

export const getTitle = (path: string): string | undefined => {
  return Object.prototype.hasOwnProperty.call(routes, path)
    ? routes[path]?.title
    : "";
};
export const getTitleOfIndex = (): string | undefined => {
  return Object.values(routes).find((element) => element?.isIndex)?.title;
};

export default routes;
