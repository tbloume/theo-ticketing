import { CategoriesType, PAPObjType, TAGObjType, TLPObjType } from "./types";

export const indexRoute = "overview";
export const categories: CategoriesType[] = ["jobs", "responders", "analyzers"];
export const defaultCategory = "jobs";

export const TLPObject: TLPObjType = [
  {
    value: 0,
    label: "white",
  },
  {
    value: 1,
    label: "green",
  },
  {
    value: 2,
    label: "amber",
  },
  {
    value: 3,
    label: "red",
  },
];

export const PAPObject: PAPObjType = TLPObject;

export const TAGObject: TAGObjType = {
  white: {
    value: "white",
    label: "white",
  },
  green: {
    value: "green",
    label: "green",
  },
  amber: {
    value: "amber",
    label: "amber",
  },
  red: {
    value: "red",
    label: "red",
  },
  lightgrey: {
    value: "lightgrey",
    label: "lightgrey",
  },
};
