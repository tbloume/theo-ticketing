import { ReactElement } from "react";

export type TLPColorsType = "green" | "red" | "amber" | "white";
export type PAPColorsType = TLPColorsType;
export type TAGColorsType = TLPColorsType | "lightgrey";

export type TLPObjType = {
  [id: number]: {
    value: 0 | 1 | 2 | 3;
    label: TLPColorsType;
  };
};

export type PAPObjType = TLPObjType;

export type TAGObjType = {
  [key in TAGColorsType]: {
    value: TAGColorsType;
    label: TAGColorsType;
  };
};

export type StatusType = "Success" | "InProgress" | "Waiting" | "Failure";

export type FormatType<T = any> = {
  [key: string]: {
    className?: string;
    name: string;
    render?: (value: any, record: T) => ReactElement;
  };
};

export type DetailsType<T = any> = Record<string, T[keyof T] | null>;

export interface JobDataOutput {
  _id: string;
  id: string;
  createdBy: string;
  updatedBy: string;
  createdAt: number;
  updatedAt: number;
  _type: string;
  type: string;
  source: string;
  sourceRef: string;
  externalLink: string;
  case: string;
  title: string;
  description: string;
  severity: number;
  date: number;
  tags: string[];
  tlp: number;
  pap: number;
  status: StatusType;
  stage: string;
  follow: boolean;
  customFields: Record<string, unknown>;
  caseTemplate?: string;
  artifacts: Array<unknown>;
  similarCases: Array<unknown>;
  customFieldValues: Record<string, unknown>;
}
export interface JobOutput {
  organization: string;
  updatedAt?: number;
  tlp: number;
  endDate: number;
  errorMessage: string;
  data: JobDataOutput;
  label: string;
  workerDefinitionId: string;
  cacheTag: string;
  pap: number;
  workerName: string;
  status: StatusType;
  createdAt: number;
  createdBy: string;
  workerId: string;
  message: string;
  dataType: string;
  type: string;
  updatedBy: string;
  startDate: number;
  parameters: { organisation: string; user: string };
  _type: string;
  _routing: string;
  _parent: Record<string, unknown>;
  _id: string;
  _seqNo: number;
  _primaryTerm: number;
  id: string;
  analyzerId: string;
  analyzerName: string;
  analyzerDefinitionId: string;
  date: number;
}
export interface ResponderOutput {
  author: string;
  name: string;
  updatedAt: number;
  url: string;
  description: string;
  baseConfig: string;
  workerDefinitionId: string;
  command: string;
  dataTypeList: string[];
  license: string;
  version: string;
  createdAt: number;
  createdBy: string;
  jobTimeout: number;
  type: string;
  updatedBy: string;
  _type: string;
  _routing: string;
  _parent: string;
  _id: string;
  _seqNo: number;
  _primaryTerm: number;
  id: string;
}
export interface AnalyzerOutput {
  author: string;
  name: string;
  updatedAt: number;
  url: string;
  description: string;
  baseConfig: string;
  workerDefinitionId: string;
  command: string;
  dataTypeList: string[];
  license: string;
  version: string;
  jobCache: number;
  createdAt: number;
  createdBy: string;
  jobTimeout: number;
  type: string;
  updatedBy: string;
  _type: string;
  _routing: string;
  _parent: string;
  _id: string;
  _seqNo: number;
  _primaryTerm: number;
  id: string;
  configuration: object; //  TODO
  analyzerDefinitionid: string;
}

export type CategoriesType = "analyzers" | "responders" | "jobs";

export type ListQueryOutput<T extends object> = { list?: T[]; total: string };
