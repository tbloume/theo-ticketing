import React, { ReactElement } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import App from "./App";
import { routes } from "./routes";

const Routing = (): ReactElement => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />}>
          {Object.entries(routes).map(([key, route]) => {
            if (!route) {
              return null;
            }
            if (route.isIndex) {
              return (
                <React.Fragment key={key}>
                  <Route index key={key} element={route.element} />
                  <Route key={key} path="/" element={route.element} />
                  <Route key={key} path={route.path} element={route.element} />
                </React.Fragment>
              );
            }
            return (
              <Route
                index={route.isIndex}
                key={key}
                path={route.path}
                element={route.element}
              />
            );
          })}
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default Routing;
