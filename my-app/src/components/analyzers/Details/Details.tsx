/* eslint-disable no-underscore-dangle */
import { Spin } from "antd";
import { useQuery } from "react-query";
import { useParams } from "react-router-dom";
import { fetchByIdAndCategory } from "../../../helper";
import { AnalyzerOutput } from "../../../types";
import "./Details.scss";

const Details = () => {
  const { id } = useParams();
  const { data, error, isError, isLoading } = useQuery<AnalyzerOutput, Error>(
    ["analyzers", id],
    () => fetchByIdAndCategory<AnalyzerOutput>(id as string, "analyzers")
  );

  if (isLoading) {
    return (
      <span>
        <Spin />
      </span>
    );
  }
  if (isError) {
    return <span>Error : {error.message}</span>;
  }
  if (!data) {
    return <span>Not happening</span>;
  }
  const { name, description, version, author, license, dataTypeList, _id } =
    data;
  return (
    <ul>
      <li> {name}</li>
      <ul>
        <li>{description}</li>
      </ul>
      <li>{version}</li>
      <li>{author}</li>
      <li>{license}</li>
      {dataTypeList.map((elem) => (
        <li key={_id}>{elem}</li>
      ))}
    </ul>
  );
};

export default Details;
