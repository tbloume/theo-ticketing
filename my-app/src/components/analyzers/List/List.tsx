import { useState } from "react";
import { useQuery } from "react-query";
import { Link } from "react-router-dom";
import { ColumnsType } from "antd/lib/table/interface";
import { Pagination, Table, Tag } from "../../UI";
import { AnalyzerOutput, ListQueryOutput } from "../../../types";
import "./List.scss";
import { fetchByCategory } from "../../../helper";
import usePagination from "../../../hooks/usePagination";

const columns: ColumnsType<AnalyzerOutput> = [
  {
    title: "Details",
    key: "details",
    width: "30%",
    render: (_, { name, description, _id }) => (
      <p className="list-details">
        <Link to={`/analyzers/${_id}`} className="list-details__name">
          {name}
        </Link>
        <span className="list-details__desc">{description}</span>
      </p>
    ),
  },
  {
    title: "Version",
    key: "version",
    width: "7%",
    render: (_, { version }) => <div className="list-version">{version}</div>,
  },
  {
    title: "Author",
    key: "author",
    width: "11%",
    render: (_, { author }) => <div className="list-author">{author}</div>,
  },
  {
    title: "License",
    key: "license",
    width: "8%",
    render: (_, { license }) => <div className="list-license">{license}</div>,
  },
  {
    title: "Applies to",
    key: "appliesto",
    width: "20%",
    render: (_, { dataTypeList }) => (
      <>
        {dataTypeList.map((data, index) => (
          <Tag
            className="list-appliesto__tag"
            value={data}
            // eslint-disable-next-line react/no-array-index-key
            key={data + index}
          />
        ))}
      </>
    ),
  },
];

const List = () => {
  const {
    currentPage,
    pageLimit,
    range,
    setCurrentPage,
    setPageLimit,
    setTotalRecords,
    totalRecords,
  } = usePagination();

  const { data, isLoading } = useQuery<any, ListQueryOutput<AnalyzerOutput>>(
    ["analyzers", range],
    () => fetchByCategory("analyzers", range),
    {
      onSuccess: (result) => {
        setTotalRecords(parseInt(result.total, 10) - 1);
      },
    }
  );

  return (
    <>
      <Table
        loading={isLoading}
        className="analyzerslist"
        dataSource={data?.list}
        columns={columns}
        scroll={{ y: "40vh" }}
        title={() => "All Analyzers"}
        rowKey="_id"
      />
      <Pagination
        align="right"
        currentPage={currentPage}
        pageLimit={pageLimit}
        totalRecords={totalRecords}
        onPreviousPage={() => setCurrentPage(currentPage - 1)}
        onNextPage={() => setCurrentPage(currentPage + 1)}
        onSelectLimit={(value: string) => {
          setCurrentPage(1);
          setPageLimit(parseInt(value, 10));
        }}
      />
    </>
  );
};

export default List;
