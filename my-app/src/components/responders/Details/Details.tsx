/* eslint-disable no-underscore-dangle */
import { useQuery } from "react-query";
import { useParams } from "react-router-dom";
import { Spin } from "antd";
import { ResponderOutput } from "../../../types";
import "./Details.scss";
import { fetchByIdAndCategory } from "../../../helper";

const Details = () => {
  const { id } = useParams();
  const { data, error, isError, isLoading } = useQuery<ResponderOutput, Error>(
    ["responders", id],
    () => fetchByIdAndCategory<ResponderOutput>(id as string, "responders")
  );
  if (isLoading) {
    return (
      <span>
        <Spin />
      </span>
    );
  }
  if (isError) {
    return <span>Error : {error.message}</span>;
  }
  if (!data) {
    return <span>Not happening</span>;
  }
  const { name, description, version, author, license, dataTypeList, _id } =
    data;

  return (
    <ul>
      <li>{name}</li>
      <ul>
        <li>{description}</li>
      </ul>
      <li>{version}</li>
      <li>{author}</li>
      <li>{license}</li>
      {dataTypeList.map((elem) => (
        <li key={_id}>{elem}</li>
      ))}
    </ul>
  );
};
export default Details;
