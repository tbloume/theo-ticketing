/* eslint-disable no-underscore-dangle */
import { useQuery } from "react-query";
import { useParams } from "react-router-dom";
import { Spin } from "antd";
import { ReactElement } from "react";
import {
  DetailsType,
  FormatType,
  JobOutput,
  TAGColorsType,
  TAGObjType,
} from "../../../types";
import {
  fetchByIdAndCategory,
  formatMDY,
  getStatusColor,
} from "../../../helper";
import { Details as UIDetails, Tag } from "../../UI";
import "./Details.scss";

const detailsFormatter: FormatType<JobOutput> = {
  title: { className: "details details--red", name: "Title key" },
  tag: {
    className: "details details--blue",
    name: "Status",
    render: (_, { status }) => (
      <Tag value={status} color={getStatusColor(status)} />
    ),
  },
  details: {
    className: "details details--green",
    name: "Details",
    render: (_, { dataType, updatedAt }) => (
      <div>
        <p>
          [{dataType}] -
          {!!updatedAt && (
            <span> Last updated {formatMDY(new Date(updatedAt))}</span>
          )}
        </p>
      </div>
    ),
  },
};

const Details = () => {
  const { id } = useParams();
  const { data, error, isError, isLoading } = useQuery<JobOutput, Error>(
    ["jobs", id],
    () => fetchByIdAndCategory<JobOutput>(id as string, "jobs")
  );

  if (isLoading || !data) {
    return (
      <span>
        <Spin />
      </span>
    );
  }
  if (isError) {
    return <span>Error : {error.message}</span>;
  }

  const { dataType, updatedAt, status, _id } = data || {};

  const objDetails: DetailsType<JobOutput> = {
    title: null,
    tag: { status },
    details: { dataType, updatedAt, status, _id },
  };

  return <UIDetails obj={objDetails} formatter={detailsFormatter} />;
};
export default Details;
