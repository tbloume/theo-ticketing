/* eslint-disable @typescript-eslint/no-explicit-any */

import { ColumnsType } from "antd/lib/table";
import { ReactElement } from "react";
import { MoreOutlined } from "@ant-design/icons";
import { useQuery } from "react-query";
import { Link } from "react-router-dom";
import { Spin } from "antd";
import { Table } from "../../UI";
import { Tag } from "../../UI/Tag";
import { JobOutput, ListQueryOutput } from "../../../types";
import { TLPObject } from "../../../constants";
import {
  formatMDY,
  formatAMPM,
  fetchByCategory,
  getStatusColor,
} from "../../../helper";
import "./List.scss";
import Pagination from "../../UI/Pagination/Pagination";
import usePagination from "../../../hooks/usePagination";

const columns: ColumnsType<JobOutput> = [
  {
    title: "Details",
    key: "details",
    width: "30%",
    render: (_, { dataType, updatedAt, label, status, _id }) => {
      return (
        <div className="list-details">
          <Tag
            className="list-details__tag"
            color={getStatusColor(status)}
            value={status}
          />
          <p className="list-details__desc">
            <Link to={`/jobs/${_id}`} className="list-details__desc-name">
              [{dataType}]{label}
            </Link>
            {!!updatedAt && (
              <span className="list-details__desc-date">
                Last updated {formatMDY(new Date(updatedAt))}
              </span>
            )}
          </p>
        </div>
      );
    },
  },
  {
    title: "User",
    key: "user",
    width: "22%",
    render: (_, { createdBy }) => (
      <div className="list-user">
        <img
          className="list-user__avatar"
          src="/icon-avatar.png"
          alt="avatar"
        />
        <a className="list-user__name">{createdBy}</a>
      </div>
    ),
  },
  {
    title: "TLP",
    key: "tlp",
    width: "7%",
    render: (_, { tlp }) => {
      return <Tag color={TLPObject[tlp].label} value={TLPObject[tlp].label} />;
    },
  },
  {
    title: "PAP",
    key: "pap",
    width: "7%",
    render: (_, { pap }) => {
      return <Tag color={TLPObject[pap].label} value={TLPObject[pap].label} />;
    },
  },
  {
    title: "Date",
    key: "date",
    width: "10%",
    render: (_, { date }) => (
      <div className="list-date">
        <a className="list-date__date">{formatMDY(new Date(date))}</a>
        <span className="list-date__time">{formatAMPM(new Date(date))}</span>
      </div>
    ),
  },

  {
    key: "action",
    width: "6%",
    render: () => (
      <a className="list-action">
        <MoreOutlined />
      </a>
    ),
  },
];

const List = (): ReactElement => {
  const {
    currentPage,
    pageLimit,
    range,
    setCurrentPage,
    setPageLimit,
    setTotalRecords,
    totalRecords,
  } = usePagination();

  const { data, isLoading } = useQuery<any, ListQueryOutput<JobOutput>>(
    ["jobs", range],
    () => fetchByCategory("jobs", range),
    {
      onSuccess: (result) => {
        setTotalRecords(parseInt(result.total, 10) - 1);
      },
    }
  );
  if (isLoading) {
    return <Spin />;
  }
  return (
    <>
      <Table<JobOutput>
        loading={isLoading}
        className="joblist"
        dataSource={data?.list || []}
        columns={columns}
        scroll={{ y: "40vh" }}
        title={() => "All Jobs"}
        rowKey="_id"
      />

      <Pagination
        align="right"
        totalRecords={totalRecords}
        currentPage={currentPage}
        pageLimit={pageLimit}
        onPreviousPage={() => setCurrentPage(currentPage - 1)}
        onNextPage={() => setCurrentPage(currentPage + 1)}
        onSelectLimit={(value: string) => {
          setCurrentPage(1);
          setPageLimit(parseInt(value, 10));
        }}
      />
    </>
  );
};

export default List;
