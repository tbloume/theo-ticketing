/* eslint-disable @typescript-eslint/no-unused-vars */
import { ReactElement, useState } from "react";
import { classList } from "../../../helper";
import { Size } from "./Size";
import { Browse } from "./Browse";

import "./Pagination.scss";

export interface PaginationProps {
  align?: "left" | "right" | "center";
  currentPage?: number;
  pageLimit: number;
  totalRecords: number;
  className?: string;
  onPreviousPage: () => void;
  onNextPage: () => void;
  onSelectLimit: (s: string) => void;
}
const Pagination = ({
  className,
  align,
  currentPage = 1,
  pageLimit = 10,
  onPreviousPage,
  onNextPage,
  onSelectLimit,
  totalRecords,
}: PaginationProps) => (
  <div
    className={classList("pagination", className, {
      [`pagination--${align}`]: !!align,
    })}
  >
    <Size onSelectLimit={onSelectLimit} pageLimit={pageLimit} />
    <Browse
      currentPage={currentPage}
      onNextPage={onNextPage}
      onPreviousPage={onPreviousPage}
      pageLimit={pageLimit}
      totalRecords={totalRecords}
    />
  </div>
);

Pagination.Browse = Browse;
Pagination.Size = Size;

export default Pagination;
