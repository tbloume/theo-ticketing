import Select from "antd/lib/select";

import "./Size.scss";

export interface PaginationSizeProps {
  pageLimit: number;
  onSelectLimit: (s: string) => void;
}
const pageLimits = [10, 30, 50, 100, 150, 300];

const Size = ({ pageLimit, onSelectLimit }: PaginationSizeProps) => {
  return (
    <div className="size">
      <span className="size__label">Items per page:</span>
      <Select
        value={pageLimit.toString()}
        className="size__select"
        onChange={(value) => onSelectLimit(value)}
        options={pageLimits.map((value) => ({
          value,
          label: value,
        }))}
      />
    </div>
  );
};

export default Size;
