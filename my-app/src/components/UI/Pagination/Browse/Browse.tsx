import { LeftOutlined, RightOutlined } from "@ant-design/icons";

import "./Browse.scss";

export interface BrowseProps {
  pageLimit: number;
  currentPage: number;
  totalRecords: number;
  onPreviousPage: () => void;
  onNextPage: () => void;
}

const Browse = ({
  currentPage,
  onNextPage,
  onPreviousPage,
  totalRecords,
  pageLimit,
}: BrowseProps) => {
  const lastIndex = currentPage * pageLimit;

  return (
    <div className="browse">
      <span className="browse__label">
        {currentPage <= 1 ? 1 : (currentPage - 1) * pageLimit + 1}-
        {lastIndex >= totalRecords ? totalRecords : lastIndex} of {totalRecords}
      </span>
      <LeftOutlined
        onClick={currentPage <= 1 ? undefined : onPreviousPage}
        className={
          currentPage <= 1
            ? "browse__arrows browse__arrows--disabled "
            : "browse__arrows"
        }
      />
      <RightOutlined
        onClick={lastIndex >= totalRecords ? undefined : onNextPage}
        className={
          lastIndex >= totalRecords
            ? "browse__arrows browse__arrows--disabled"
            : "browse__arrows"
        }
      />
    </div>
  );
};

export default Browse;
