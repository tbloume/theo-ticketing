import { FormatType } from "../../../types";
import "./Details.scss";

interface DetailProps<T = any> {
  obj: Record<string, unknown>;
  formatter: FormatType<T>;
}
const Details = ({ obj, formatter }: DetailProps) => {
  console.log(formatter);

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        textAlign: "center",
      }}
    >
      {Object.entries(obj).map(([key, value]) => (
        <div
          key={key}
          className={formatter[key].className}
          style={{
            display: "flex",
            flexDirection: "column",
            flex: "1",
          }}
        >
          <div
            style={{
              fontWeight: "bold",
              marginBottom: "2rem",
            }}
          >
            {formatter[key].name}
          </div>

          <div>
            {formatter[key].render && formatter[key].render?.(key, value)}
            {!formatter[key].render && key}
          </div>
        </div>
      ))}
    </div>
  );
};

export default Details;
