/* eslint-disable react/prop-types */
import { ReactElement } from "react";
import { Table as AntTable, TableProps } from "antd";
import "./Table.scss";
import { classList } from "../../../helper";

const Table = <T extends object>({
  dataSource,
  columns,
  className,
  scroll,
  title,
  loading,
}: TableProps<T>): ReactElement => {
  return (
    <AntTable<T>
      className={classList("table", { [className as string]: !!className })}
      dataSource={dataSource}
      columns={columns}
      scroll={scroll}
      title={title}
      rowKey="_id"
      pagination={false}
      loading={loading}
    />
  );
};

export default Table;
