/* eslint-disable react/require-default-props */
import React from "react";
import "./Tag.scss";
import { Tag as AntTag } from "antd";
import { classList } from "../../../helper";
import { TAGColorsType } from "../../../types";

export interface TagProps {
  value: string;
  color?: TAGColorsType;
  size?: "small" | "regular"; // large
  className?: string;
}

const Tag = ({ value, color, size = "regular", className }: TagProps) => {
  return (
    <AntTag
      className={classList(
        "tag",
        className,
        { [`tag--${color}`]: !!color },
        { [`tag--${size}`]: !!size }
      )}
    >
      {value.toUpperCase()}
    </AntTag>
  );
};

export default Tag;
