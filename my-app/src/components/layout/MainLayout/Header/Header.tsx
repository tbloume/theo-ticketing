/* eslint-disable jsx-a11y/anchor-is-valid */
import { NotificationFilled } from "@ant-design/icons";
import { Avatar, Divider, Layout } from "antd";

import useTitle from "../../../../hooks/useTitle";

import ModalForm from "../Analysis/Analysis";
import "./Header.scss";

const { Header: AntHeader } = Layout;

const Header = () => {
  const { title } = useTitle();
  return (
    <AntHeader style={{ padding: "0" }} className="header">
      <div className="header__content">
        <h1 className="header__title">{title}</h1>
        <ModalForm className="header__analysis" />
        <div className="header__infos">
          <NotificationFilled className="header__notif-icon" />
          <Divider type="vertical" className="header__divider" />
          <span className="header__username">Théo Bloume</span>
          <Avatar
            className="header__avatar"
            size={{ xs: 24, sm: 32, md: 40, lg: 40, xl: 40, xxl: 40 }}
            src="/icon-avatar.png"
          />
        </div>
      </div>
    </AntHeader>
  );
};
export default Header;
