import "./MainLayout.scss";
import { Layout } from "antd";
import React, { ReactElement } from "react";
import { Outlet } from "react-router-dom";
import Menu from "../Menu/Menu";
import { Header } from "./Header";

const { Content, Footer, Sider } = Layout;
const MainLayout = (): ReactElement => (
  <Layout className="main-layout">
    <Sider>
      <Menu />
    </Sider>
    <Layout>
      <Header />
      <Content style={{ margin: "24px 16px 0" }}>
        <Outlet />
      </Content>
      <Footer style={{ textAlign: "center" }}>StrangeBee Théo ©2022</Footer>
    </Layout>
  </Layout>
);

export default MainLayout;
