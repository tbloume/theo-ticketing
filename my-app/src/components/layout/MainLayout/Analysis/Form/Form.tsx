import { Form as AntForm, Modal } from "antd";
import { FormItemProps } from "antd/es/form/FormItem";
import { ReactElement } from "react";
import { AnalysisValues as FormValues } from "../Analysis";
import "./Form.scss";

interface CreateFormProps {
  open: boolean;
  onCreate: (values: FormValues) => void;
  onCancel: () => void;
  items: FormItemProps[];
}

const Form = ({
  open,
  onCancel,
  onCreate,
  items,
}: CreateFormProps): ReactElement => {
  const [form] = AntForm.useForm();
  return (
    <Modal
      open={open}
      title="Run analysis"
      okText="Create"
      cancelText="Cancel"
      onCancel={() => {
        onCancel();
        form.resetFields();
      }}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            form.resetFields();
            onCreate(values);
          })
          .catch((info) => {
            console.log("Validate Failed:", info);
          });
      }}
    >
      <AntForm
        form={form}
        layout="horizontal"
        name="form_in_modal"
        labelCol={{ span: 5 }}
        method="post"
        wrapperCol={{ span: 25 }}
      >
        {items.map((item) => (
          <AntForm.Item
            className={item.className}
            key={item.fieldId}
            name={item.name}
            label={item.label}
            initialValue={item.initialValue}
            rules={item.rules}
          >
            {item.children}
          </AntForm.Item>
        ))}
      </AntForm>
    </Modal>
  );
};
export default Form;
