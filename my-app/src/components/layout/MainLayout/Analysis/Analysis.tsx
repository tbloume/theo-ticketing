import React, { useMemo, useState } from "react";
import { Button, Checkbox, Input, Select, Upload } from "antd";
import { useQuery, useQueryClient } from "react-query";
import { FormItemProps } from "antd/es/form/FormItem";
import { DefaultOptionType } from "antd/lib/select";
import { InboxOutlined } from "@ant-design/icons";
import {
  AnalyzerOutput,
  ListQueryOutput,
  PAPColorsType,
  TLPColorsType,
} from "../../../../types";
import { classList, fetchByCategory } from "../../../../helper";
import "./Analysis.scss";
import { PAPObject, TLPObject } from "../../../../constants";
import { Form } from "./Form";
import { cortexInstance } from "../../../../config";

export interface AnalysisValues {
  tlp: TLPColorsType;
  pap: PAPColorsType;
  dataType: string[];
  data: string;
  file: { file: File; fileList: File[] };
  analyzersName: string[];
}

interface AnalysisProps {
  className?: string;
}

const { Dragger } = Upload;

const Analysis = ({ className }: AnalysisProps) => {
  const [open, setOpen] = useState(false);
  const [currentVal, setCurrentVal] = useState("");
  const [Analyzers, setAnalyzers] = useState<AnalyzerOutput[]>([]);

  const queryClient = useQueryClient();
  const isFile = currentVal === "file";

  const { data } = useQuery<any, ListQueryOutput<AnalyzerOutput>>(
    ["analyzers", "all"],
    () => fetchByCategory("analyzers", "all"),
    { enabled: !!open }
  );

  const dataTypesList: DefaultOptionType[] = useMemo(
    () =>
      Array.from<string>(
        new Set(data?.list.flatMap((d: AnalyzerOutput) => d.dataTypeList))
      )
        .sort()
        .map((item) => ({ value: item, label: item })),
    [data?.list]
  );

  const onChange = (value: string) => {
    setCurrentVal(value);
    setAnalyzers(
      data?.list.filter((analyzer: AnalyzerOutput) =>
        analyzer.dataTypeList.includes(value)
      )
    );
  };

  const onClose = () => {
    setOpen(false);
    setCurrentVal("");
    setAnalyzers([]);
  };

  const reqAnalysis = (values: AnalysisValues, params: any) => {
    values.analyzersName.forEach((name) => {
      const id = Analyzers?.find((analyzer: AnalyzerOutput) => {
        return analyzer.name === name;
      })?.id;
      cortexInstance
        .post(`analyzer/${id}/run`, params)
        .then((response) => {
          console.log(response);
          queryClient.refetchQueries({ queryKey: ["jobs"] });
        })
        .catch((error) => {
          return error;
        });
    });
  };
  const onCreate = (values: AnalysisValues) => {
    console.log(values);
    if (isFile) {
      const formData = new FormData();
      formData.append("attachment", values.file.file);
      formData.append(
        "_json",
        JSON.stringify({
          dataType: values.dataType,
          tlp: values.tlp,
          pap: values.pap,
        })
      );
      reqAnalysis(values, formData);
    } else {
      const params = {
        data: values.data,
        attributes: {
          dataType: values.dataType,
          tlp: values.tlp,
          pap: values.pap,
        },
      };
      reqAnalysis(values, params);
    }
    onClose();
  };

  const items: FormItemProps[] = [
    {
      fieldId: "tlp",
      name: "tlp",
      label: "TLP",
      initialValue: "amber",
      rules: [
        {
          required: true,
          message: "Please choose TLP !",
        },
      ],
      children: (
        <Select
          className="analysis__field "
          options={Object.values(TLPObject)}
        />
      ),
    },
    {
      fieldId: "pap",
      name: "pap",
      label: "PAP",
      initialValue: "amber",
      rules: [
        {
          required: true,
          message: "Please choose PAP !",
        },
      ],
      children: (
        <Select
          className="analysis__field "
          options={Object.values(PAPObject)}
        />
      ),
    },
    {
      fieldId: "dataType",
      name: "dataType",
      label: "DataType ",
      rules: [
        {
          required: true,
          message: "Please choose data type !",
        },
      ],
      children: (
        <Select
          showSearch
          className="analysis__field "
          onChange={(t) => onChange(t)}
          placeholder="--choose data type--"
          optionFilterProp="children"
          filterOption={(input, option) =>
            `${option?.value}`?.toLowerCase().includes(input.toLowerCase())
          }
          options={dataTypesList}
        />
      ),
    },
    {
      name: "data",
      label: "Data",
      fieldId: "data",
      className: classList({ hidden: isFile }),
      rules: [
        {
          required: !isFile,
          message: "Please input data !",
        },
      ],
      children: (
        <Input
          className="analysis__field "
          placeholder="Data: 8.8.8.8, test.com"
        />
      ),
    },
    {
      name: "file",
      label: "File",
      fieldId: "file",
      validateStatus: "success",
      className: classList({ hidden: !isFile }),
      rules: [
        {
          required: isFile,
          message: "Please input file !",
        },
      ],
      children: (
        <Dragger
          name="file"
          multiple={false}
          maxCount={1}
          beforeUpload={() => false}
        >
          <p className="ant-upload-drag-icon">
            <InboxOutlined />
          </p>
          <p className="ant-upload-text">
            Click or drag file to this area to upload
          </p>
        </Dragger>
      ),
    },
    {
      name: "analyzersName",
      label: "Analyzers",
      fieldId: "analyzersName",
      className: classList({ hidden: !(Analyzers?.length > 0) }),

      rules: [
        {
          required: true,
          message: "Please one or multiple analyzer(s) !",
        },
      ],
      children: (
        <Checkbox.Group className="analysis__checkboxes">
          {Analyzers?.map((analyzer: AnalyzerOutput) => (
            <Checkbox
              className="analysis__checkbox"
              value={analyzer.name}
              key={analyzer.name}
            >
              {analyzer.name}
            </Checkbox>
          ))}
        </Checkbox.Group>
      ),
    },
  ];
  return (
    <div className={className}>
      <Button
        type="primary"
        onClick={() => {
          setOpen(true);
        }}
      >
        New Analysis
      </Button>
      <Form open={open} onCreate={onCreate} onCancel={onClose} items={items} />
    </div>
  );
};

export default Analysis;
