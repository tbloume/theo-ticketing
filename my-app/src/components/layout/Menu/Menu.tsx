import { Link, useLocation, useNavigate } from "react-router-dom";
import { Menu as AntMenu } from "antd";
import { ReactElement, useMemo } from "react";
import { ItemType } from "antd/lib/menu/hooks/useItems";
import "./Menu.scss";
import routes from "../../../routes/routes";
import { indexRoute } from "../../../constants";
import useBasePath from "../../../hooks/useBasePath";

const Menu = (): ReactElement => {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const basePath = useBasePath(pathname);
  const currentLocation = pathname === "/" ? indexRoute : basePath;

  const items: ItemType[] = useMemo(
    () =>
      Object.entries(routes).map(([key, route]) => {
        if (!route) {
          return {
            type: "divider",
          };
        }
        if (!route.isMenuItem) {
          return null;
        }
        return {
          label: route.title,
          key,
          icon: route.icon,
          onClick: () => navigate(route.path),
        };
      }),
    [navigate]
  );

  return (
    <>
      <div className="menu-logo">
        <Link to={indexRoute}>
          <img src="/logo192.png" alt="logo" className="menu-logo__img" />
        </Link>
        <Link to={indexRoute} className="menu-logo__title">
          TT
        </Link>
      </div>
      <AntMenu
        mode="inline"
        selectedKeys={[currentLocation]}
        items={items}
        className="menu"
      />
    </>
  );
};

export default Menu;
