import {
  CategoriesType,
  ListQueryOutput,
  StatusType,
  TAGColorsType,
} from "./types";
import { cortexInstance } from "./config";
import { TAGObject } from "./constants";

export const CapitalizeFirstLetter = (str: string) =>
  str.charAt(0).toUpperCase() + str.slice(1);

export const getStatusColor = (status: StatusType): TAGColorsType => {
  if (status === "InProgress" || status === "Waiting") {
    return TAGObject.lightgrey.label;
  }
  if (status === "Success") {
    return TAGObject.green.label;
  }
  return TAGObject.red.label;
};

export const formatMDY = (d: Date) => {
  let ret = "";
  ret += new Intl.DateTimeFormat("en-US", { month: "long" })
    .format(d)
    .substring(0, 3);
  ret += ` ${d.getDate()}`;
  ret += ` ${d.getFullYear()}`;
  return ret;
};

export const formatAMPM = (d: Date) => {
  let hours = d.getHours();
  const minutes = d.getMinutes();
  const ampm = hours >= 12 ? "pm" : "am";
  hours %= 12;
  hours = hours || 12;
  const strTime = `${hours}:${minutes} ${ampm}`;
  return strTime;
};

export const classList = (
  ...cs: (Record<string, boolean> | string | undefined)[]
): string =>
  cs
    .reduce<string>((acc, c) => {
      if (!c) return acc;
      const parsedValue =
        typeof c === "string"
          ? c
          : Object.keys(c)
              .filter((key) => !!c[key])
              .join(" ");
      return `${acc} ${parsedValue}`;
    }, "")
    .trim();

export const fetchByCategory = <T extends object>(
  category: CategoriesType,
  range = "0-10"
): Promise<ListQueryOutput<T>> => {
  // dangerous
  const c = category.slice(0, -1);
  //
  return cortexInstance
    .post(`${c}/_search?range=${range}&sort=-createdAt`, {})
    .then((response) => {
      return { list: response.data, total: response.headers["x-total"] };
    })
    .catch((error) => {
      return error;
    });
};

export const fetchByIdAndCategory = <T extends object>(
  id: string,
  category: CategoriesType
): Promise<T> => {
  //  dangerous
  const c = category.slice(0, -1);
  //
  return cortexInstance
    .get(`${c}/${id}${c === "job" ? "/report" : ""}`, {})
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return error;
    });
};
