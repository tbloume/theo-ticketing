# Getting Started with the project

Install dependencies
### `npm install`

Run the app
### `npm start`

Run storybook
### `npm run storybook`

